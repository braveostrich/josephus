﻿using System;

namespace Josephus
{
    public class Program
    {
        // The application should run as "josephus.exe 6 3"
        public static void Main(string[] args)
        {
            if (args.Length != 2)
            {
                Console.Out.WriteLine("Please provide the parameters as 'n k'(without quote mark).");
                return;
            }
            var game = new Game();
            var textGameReporter = new TextGameReporter();
            game.AppendReporter(textGameReporter);
            game.Run(Convert.ToInt32(args[0]), Convert.ToInt32(args[1]));
            Console.Out.WriteLine(textGameReporter.ToString());
        }
    }
}
