namespace Josephus
{
    public interface Identifiable
    {
        int Id { get; }
    }
}