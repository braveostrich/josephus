namespace Josephus
{
    public class Child : Identifiable
    {
        public Child(int id)
        {
            Id = id;
        }

        public int Id { get; private set; }

    }
}