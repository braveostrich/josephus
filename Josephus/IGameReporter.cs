namespace Josephus
{
    public interface IGameReporter
    {
        void OnEliminating(int id);
        void OnDecidingWinner(int winnerId);
    }
}