using System;

namespace Josephus
{
    public class Cursor
    {
        // We store _previous instead of current because when eliminating current child we need to 
        // change the Next of previous. Otherwise we will have lost the previous reference.
        private IterableChild _previous = IterableChild.Outlier;

        public Cursor(IterableChild head)
        {
            _previous.Next = head;
        }

        public IterableChild Value
        {
            get { return _previous.Next; }
        }

        public IterableChild EliminateMe()
        {
            if (Isolated())
            {
                throw new InvalidOperationException("Trying to eliminate the value under an isolated cursor.");
            }
            var me = Value;
            _previous.Next = me.Next;
            return me;
        }

        public void SetNext(IterableChild next)
        {
            _previous.Next.Next = next;
        }

        public void Move()
        {
            if (Value.Next == null)
            {
                throw new InvalidOperationException("Cannot move when Next item is null.");
            }
            _previous = Value;
        }

        private bool Isolated()
        {
            return Value.Next == null || Value.Next == Value;
        }
    }
}