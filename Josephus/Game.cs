using System.Collections.Generic;

namespace Josephus
{
    public class Game
    {
        private readonly List<IGameReporter> gameReporters = new List<IGameReporter>();

        public void AppendReporter(IGameReporter gameReporter)
        {
            gameReporters.Add(gameReporter);
        }

        public void Run(int numberOfChildren, int k)
        {
            var circle = new Circle(numberOfChildren, k);
            while (!circle.WinnerDecided())
            {
                var eliminated = circle.EliminateOne();
                gameReporters.ForEach(reporter => reporter.OnEliminating(eliminated));
            }
            gameReporters.ForEach(reporter => reporter.OnDecidingWinner(circle.GetWinnerId()));
        }
    }
}