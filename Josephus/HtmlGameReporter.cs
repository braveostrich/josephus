using System.Text;

namespace Josephus
{
    public class HtmlGameReporter:IGameReporter
    {
        private readonly StringBuilder reportBuilder = new StringBuilder("The elimination sequence is");


        public void OnEliminating(int id)
        {
            reportBuilder.Append(string.Format(" {0},", id));
        }

        public void OnDecidingWinner(int winnerId)
        {
            reportBuilder.Append(string.Format(" {0}.<br/>", winnerId));
            reportBuilder.Append(string.Format("The winner's Id is <strong>{0}</strong>.", winnerId));
        }

        public override string ToString()
        {
            return reportBuilder.ToString();
        }
    }
}