﻿namespace Josephus
{
    public class JosephusON
    {
        // The O(n) algorithm
        public static int getWinner(int n, int k)
        {
            var r = 0;
            var i = 2;
            while (i <= n)
            {
                r = (r + k) % i;
                i += 1;
            }
            return r + 1;
        }

    }
}
