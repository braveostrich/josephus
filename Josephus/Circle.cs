using System;

namespace Josephus
{

    public class Circle
    {
        private readonly int _k;
        private readonly Cursor _cursor;
        private int _n;

        public Circle(int n, int k)
        {
            VerifyParameterContract(n > 0, k > 0);
            _n = n;
            _k = k;
            var headChild = new IterableChild(1);

            _cursor = new Cursor(headChild);
            for (var i = 2; i <= n; i++)
            {
                _cursor.SetNext(new IterableChild(i));
                _cursor.Move();
            }
            _cursor.SetNext(headChild);

            // Move Cursor to the head.
            _cursor.Move();
        }


        public int EliminateOne()
        {
            if (WinnerDecided())
            {
                throw new InvalidOperationException("The winner should not be eliminated. Always check WinnerDecided before elimination.");
            }

            // Performance improved for big _k.
            var steps = (((_k - 1)%_n) + 1);
            for (var i = 1; i < steps; i++)
            {
                _cursor.Move();
            }
            var me = _cursor.EliminateMe();
            _n--;
            return me.Id;
        }

        public bool WinnerDecided()
        {
            return _n == 1;
        }

        public int GetWinnerId()
        {
            if(!WinnerDecided())
            {
                throw new InvalidOperationException("Always check WinnerDecided before calling GetWinnerId.");
            }
            return _cursor.Value.Id;
        }

        // ReSharper disable NotResolvedInText
        private static void VerifyParameterContract(bool nGreaterThan0, bool kGreaterThan0)
        {
            // Uncomment the following line if Code Contract is installed
            // Contract.Requires<ArgumentOutOfRangeException>(n > 0, "Initializing a circle with n <= 0 is illeagle.");
            if (!nGreaterThan0)
            {
                throw new ArgumentOutOfRangeException("n", "Initializing a circle with n <= 0 is illeagle.");
            }
            if (!kGreaterThan0)
            {
                throw new ArgumentOutOfRangeException("k", "We cannot eliminate any child when k <= 0.");
            }
        }
        // ReSharper restore NotResolvedInText
    }
}