namespace Josephus
{
    public class IterableChild : Identifiable
    {
        private static readonly IterableChild _outlier = new IterableChild(-1);
        
        private readonly Child _child;

        public IterableChild(int id)
        {
            _child = new Child(id);
        }


        public IterableChild Next { get; set; }

        public int Id
        {
            get { return _child.Id; }
        }

        public static IterableChild Outlier
        {
            get { return _outlier; }
        }
    }
}