# READ ME
## Setting up development environment
### Installations
+ VS 2010
+ Reshaper 6.1.1000.82
+ GallioBundle-3.4.14.0. (Latest GallioBundle only works with Resharper 6.1.)

### Dependencies(Already included in the 'Tools' folder)
+ [Test dependency] Concordion-latest. (Mannually rebuilt fixing a defect.)
+ [Test dependency] xUnit-1.9.1 with xUnit extensions.
+ [Development dependency] Gallio.ConcordionAdapter.dll. (Mannually built against GallioBundle-3.4.14.0. The built-in adapter with Concordion was built against GallioBundle 3.2.)

### Configuration
+  Add Concordion Adapter to Gallio as Runtime plugin.
> Open Gallio Control Panel.
> Add Gallio.ConcordionAdapter.dll in `Gallio->Runtime panel`.

+  Disable `Shadow-copy` in Resharper. Otherwise Concordion report will be output to your %temp% folder.
> Open Resharper->Option from menu.
> Go to `Tools->Unit Testing` tab.
> Uncheck `Shadow-copy assemblies being tested`.


## Build the solution
Open the solution with Visual Studio 2010, select `Build Solution` (Ctrl+Alt+B) to build the solution.

## Run all tests
Select `Run Unit Tests` from the context menu of "Solution 'homework'".

Concordion Specification Report will be output in your `bin/Debug/Spec` folder. A prepared report is attached with the source code.