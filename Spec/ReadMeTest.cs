﻿using Concordion.Integration;

namespace Spec
{
    [ConcordionTest]
    public class ReadMeTest
    {
        public string greetingFor(string firstName)
        {
            return string.Format("Hello {0}!", firstName);
        }
    }
}
