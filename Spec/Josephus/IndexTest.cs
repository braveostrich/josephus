﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Concordion.Integration;
using Josephus;

namespace Spec.Josephus
{
    [ConcordionTest]
    public class IndexTest
    {
        private Circle _circle;

        public String getEliminatingSequence()
        {
            _circle = new Circle(6, 3);
            var resultBuilder = new StringBuilder(6);
            while (!_circle.WinnerDecided())
            {
                resultBuilder.Append(_circle.EliminateOne());
                resultBuilder.Append(", ");
            }
            resultBuilder.Append(_circle.GetWinnerId());
            return resultBuilder.ToString();
        }

        public String getWinningChildId()
        {
            return _circle.GetWinnerId().ToString(CultureInfo.InvariantCulture);
        }
    }
}