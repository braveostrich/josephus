﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Concordion.Integration;
using Josephus;

namespace Spec.Josephus
{
    [ConcordionTest]
    public class MoreTest
    {
        public Result getResultFor(string k)
        {
            var circle = new Circle(1, Convert.ToInt32(k));
            return BuildResult(circle);
        }

        public Result getResultFor(string n, string k)
        {
            var circle = new Circle(Convert.ToInt32(n), Convert.ToInt32(k));
            return BuildResult(circle);
        }

        private static Result BuildResult(Circle circle)
        {
            var resultBuilder = new StringBuilder(6);

            while (!circle.WinnerDecided())
            {
                resultBuilder.Append(string.Format(" {0},",circle.EliminateOne()));
            }
            resultBuilder.Append(string.Format(" {0}", circle.GetWinnerId()));
            return new Result
                       {
                           sequence = resultBuilder.ToString(),
                           winner = circle.GetWinnerId().ToString(CultureInfo.InvariantCulture)
                       };
        }
    }
}