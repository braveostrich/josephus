﻿using System;
using System.Diagnostics;
using System.Globalization;
using Concordion.Integration;
using Josephus;

namespace Spec.Josephus
{
    [ConcordionTest]
    public class ComparisonTest
    {
        public string timeConsumed(string n, string k, string times)
        {
            var numberOfChildren = Convert.ToInt32(n);
            var eliminationParamenter = Convert.ToInt32(k);
            var playTimes = Convert.ToInt32(times);
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            for (var i = 0; i < playTimes; i++)
            {
                var circle = new Circle(numberOfChildren, eliminationParamenter);
                // We use 'for' instead of 'while circle.WinnerDecided' as loop condition to avoid some checking time
                for (var j = 0; j < numberOfChildren - 1; j++)
                {
                    circle.EliminateOne();
                }
                circle.GetWinnerId();
            }
            stopwatch.Stop();
            return stopwatch.ElapsedMilliseconds.ToString(CultureInfo.InvariantCulture);
        }

        public string timeConsumedOn(string n, string k, string times)
        {
            var numberOfChildren = Convert.ToInt32(n);
            var eliminationParamenter = Convert.ToInt32(k);
            var playTimes = Convert.ToInt32(times);

            var stopwatch = new Stopwatch();
            stopwatch.Start();
            for (var i = 0; i < playTimes; i++)
            {
                JosephusON.getWinner(numberOfChildren, eliminationParamenter);
            }
            stopwatch.Stop();
            return stopwatch.ElapsedMilliseconds.ToString(CultureInfo.InvariantCulture);
        }
    }
}
