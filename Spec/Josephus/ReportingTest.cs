﻿using Concordion.Integration;
using Josephus;

namespace Spec.Josephus
{
    [ConcordionTest]
    public class ReportingTest
    {
        private readonly TextGameReporter _textGameReporter = new TextGameReporter();
        private readonly HtmlGameReporter _htmlGameReporter = new HtmlGameReporter();

        public ReportingTest()
        {
            var game = new Game();
            game.AppendReporter(_textGameReporter);
            game.AppendReporter(_htmlGameReporter);
            game.Run(6, 2);
            
        }

        public string getReportWithTextReporter()
        {
            return _textGameReporter.ToString();
        }

        public string getReportWithHtmlReporter()
        {
            return _htmlGameReporter.ToString();
        }
    }
}
