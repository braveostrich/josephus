using System;

namespace Spec.Josephus
{
    public class Result
    {
        public String sequence;
        public String winner;
    }
}