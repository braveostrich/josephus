﻿using Josephus;
using Xunit;

namespace Test
{
    public class ReportingTest
    {
        private readonly Game _game = new Game();

        [Fact]
        public void should_be_able_to_report_with_a_text_reporter()
        {
            var textGameReporter = new TextGameReporter();
            _game.AppendReporter(textGameReporter);
            _game.Run(6, 2);
            Assert.Equal("The elimination sequence is 2, 4, 6, 3, 1, 5.\nThe winner's Id is *5*.", 
                textGameReporter.ToString());
        }

        [Fact]
        public void should_be_able_to_report_with_an_html_reporter()
        {
            var htmlGameReporter = new HtmlGameReporter();
            _game.AppendReporter(htmlGameReporter);
            _game.Run(6, 2);
            Assert.Equal("The elimination sequence is 2, 4, 6, 3, 1, 5.<br/>The winner's Id is <strong>5</strong>.", 
                htmlGameReporter.ToString());
        }
    }
}
