﻿using System;
using Josephus;
using Xunit;


namespace Test
{
    public class CursorTest
    {
        [Fact]
        public void cursor_should_point_to_head_after_initialization()
        {
            var head = new IterableChild(1);
            var cursor = new Cursor(head);
            Assert.Equal(head, cursor.Value);

            var second = new IterableChild(2);
            cursor.SetNext(second);
            Assert.Equal(head, cursor.Value);
        }

        [Fact]
        public void throw_exception_when_try_to_move_and_next_is_null()
        {
            var head = new IterableChild(1);
            var cursor = new Cursor(head);

            Assert.Throws<InvalidOperationException>(()=>cursor.Move());
        }

        [Fact]
        public void throw_exception_when_eliminating_item_from_an_isolated_cursor()
        {
            var head = new IterableChild(1);
            var cursor = new Cursor(head);

            Assert.Throws<InvalidOperationException>(() => cursor.EliminateMe());
            
            var second = new IterableChild(2);
            cursor.SetNext(second);
            cursor.EliminateMe();

            Assert.Throws<InvalidOperationException>(() => cursor.EliminateMe());
        }
        [Fact]
        public void throw_exception_when_eliminating_item_from_an_isolated_cursor2()
        {
            var head = new IterableChild(1);
            var cursor = new Cursor(head);

            var second = new IterableChild(2);
            cursor.SetNext(second);

            cursor.EliminateMe();

            Assert.Throws<InvalidOperationException>(() => cursor.EliminateMe());
        }
    }
}
