﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Josephus;
using Xunit;

namespace Test
{
    public class HowFastIsIt
    {

        [Fact]
        public void time_consumed()
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            for (var i = 0; i < 10; i++)
            {
                var circle = new Circle(1000, 7000);

                for (var j = 0; j < 1000 - 1; j++)
                {
                    circle.EliminateOne();
                }
                circle.GetWinnerId();
            }
            stopwatch.Stop();
            Console.Out.WriteLine(stopwatch.ElapsedMilliseconds);
        }

        [Fact]
        public void time_consumed_ON_algorithm()
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            for (var i = 0; i < 1000; i++)
            {
                JosephusON.getWinner(1000, 7);
            }
            stopwatch.Stop();
            Console.Out.WriteLine(stopwatch.ElapsedMilliseconds);
        }

    }
}