﻿using System;
using System.IO;
using Xunit;

namespace Test
{
    public class MainTest
    {
        [Fact]
        public void Demo()
        {
            using (var stringWriter = new StringWriter())
            {
                Console.SetOut(stringWriter);
                Josephus.Program.Main(new[] { "6", "3" });

                const string expected = "The elimination sequence is 3, 6, 4, 2, 5, 1.\nThe winner's Id is *1*.\r\n";
                Assert.Equal(expected, stringWriter.ToString());
            }
        }

        [Fact]
        public void ErrorHandling()
        {

            using (var stringWriter = new StringWriter())
            {
                Console.SetOut(stringWriter);
                Josephus.Program.Main(new[] { "s0m4w1rd charact4rs" });

                const string expected = "Please provide the parameters as 'n k'(without quote mark).\r\n";
                Assert.Equal(expected, stringWriter.ToString());
            }
        }
    }
}
