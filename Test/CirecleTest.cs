﻿using System;
using System.Collections.Generic;
using System.Text;
using Josephus;
using Xunit;
using Xunit.Extensions;

namespace Test
{
    public class CirecleTest
    {
        [Theory,
         InlineData(1),
         InlineData(2),
         InlineData(100),
         InlineData(100000)]
        public void should_be_able_to_create_a_circle_with_any_natural_number(int numberOfChildren)
        {
            new Circle(numberOfChildren, 3);
        }

        [Theory,
         InlineData(1, true),
         InlineData(2, false),
         InlineData(3, false)
        ]
        public void circle_with_only_one_child_is_winner_decided_after_initialization(int numberOfChildren, bool winnerDecided)
        {
            var circle = new Circle(numberOfChildren, 3);
            Assert.Equal(winnerDecided, circle.WinnerDecided());
        }

        [Theory,
         InlineData(3, 1, "123"),
         InlineData(6, 1, "123456"),
         InlineData(4, 2, "2431"),
         InlineData(2, 4, "21")
        ]
        public void should_be_able_to_eliminate_one_by_one(int numberOfChildren, int eliminationParameter,
                                                           string eliminationSequence)
        {
            var circle = new Circle(numberOfChildren, eliminationParameter);
            var eliminationSequenceBuilder = new StringBuilder();
            while (!circle.WinnerDecided())
            {
                eliminationSequenceBuilder.Append(circle.EliminateOne());
            }
            eliminationSequenceBuilder.Append(circle.GetWinnerId());

            var josephusWinnerId = JosephusON.getWinner(numberOfChildren, eliminationParameter);
            Assert.Equal(josephusWinnerId, circle.GetWinnerId());
            Assert.Equal(eliminationSequence, eliminationSequenceBuilder.ToString());
        }


        [Fact]
        public void should_thrwo_exception_when_trying_to_initialize_circel_with_n_less_than_1()
        {
            var illeagleN = Assert.Throws<ArgumentOutOfRangeException>(() => { new Circle(-1, 3); });
            const string exceptionMessage = "Initializing a circle with n <= 0 is illeagle.";
            var expectedException = new ArgumentOutOfRangeException("n", exceptionMessage);
            Assert.Equal(expectedException, illeagleN, new ExceptionMessageComparator());
        }

        [Fact]
        public void should_thrwo_exception_when_trying_to_initialize_circel_with_k_less_than_1()
        {
            var illeagalK = Assert.Throws<ArgumentOutOfRangeException>(() => { new Circle(2, -1); });
            const string exceptionMessage = "We cannot eliminate any child when k <= 0.";
            var expectedException = new ArgumentOutOfRangeException("k", exceptionMessage);
            Assert.Equal(expectedException, illeagalK, new ExceptionMessageComparator());
        }

        [Fact]
        public void the_winner_should_not_be_eliminated()
        {
            var circle = new Circle(1, 1);
            var eliminatingWinnerException = Assert.Throws<InvalidOperationException>(() => circle.EliminateOne());
            var expectedException =
                new InvalidOperationException("The winner should not be eliminated. Always check WinnerDecided before elimination.");
            Assert.Equal(expectedException, eliminatingWinnerException, new ExceptionMessageComparator());
        }


        [Fact]
        public void should_throw_exception_when_trying_to_get_winner_id_before_it_has_been_decided()
        {
            var circle = new Circle(4, 3);
            Assert.Throws<InvalidOperationException>(() => circle.GetWinnerId());
        }
    }

    public class ExceptionMessageComparator : IEqualityComparer<Exception>
    {
        public bool Equals(Exception x, Exception y)
        {
            return x.Message == y.Message;
        }

        public int GetHashCode(Exception obj)
        {
            return obj.GetHashCode() + 37;
        }
    }
}